var $$ = Dom7;
var api_url = "http://13.113.64.137/apure_api/data.php";
var stores_hit = [];
var stores_label = [];
var background_color = [];
$$.get(api_url, function(data, status, xhr){
    var resp = JSON.parse(xhr.response);
    $$('.total_hits').text(resp['stores_hit'][0]['store_all'] + " hits in total");
    var i = 0;
    for(var key in resp['stores_hit'][0]){
        if(key!="store_all"){
            stores_hit[i]=resp['stores_hit'][0][key];
            key = key.split('_');
            stores_label[i]=key[1];
            background_color[i++]="rgba(83,170,108, 1)";
            //console.log(stores_hit);
        }
    }
    var ctx = document.getElementById("myChart");
    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: {
            labels: stores_label,
            datasets: [{
                label: 'Number of Hits',
                data: stores_hit,
                backgroundColor: background_color,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }],
                xAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });
});
